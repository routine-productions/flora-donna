(function ($) {
    var lightbox = $('.Lightbox-Wrap'),
        viewItem = $('.Lightbox-View img'),
        lightboxImg = $('.Teaser-Image');
    $('body').click(function (e) {
        if (!$(e.target).is('.Lightbox-View img')) {
            lightbox.removeClass('Box-Active');
        }
    });

    lightboxImg.click( function (event) {
        console.log('clicked!');
        if (lightbox.hasClass('Box-Active')) {
            lightbox.removeClass('Box-Active');

        }
        else {
            lightbox.addClass('Box-Active');
            viewItem.attr('src', ($(this).find('img').attr("src")));
        }
        return false;
    });


})(jQuery);