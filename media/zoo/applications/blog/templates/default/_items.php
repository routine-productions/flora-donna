<?php
/**
 * @package   com_zoo
 * @author    YOOtheme http://www.yootheme.com
 * @copyright Copyright (C) YOOtheme GmbH
 * @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');




// init vars
$i = 0;
$columns = [];
$column = 0;
$row = 0;
$rows = ceil(count($this->items) / $this->params->get('template.items_cols'));

// create columns
foreach ($this->items as $item) {

    if ($this->params->get('template.items_order')) {
        // order down
        if ($row >= $rows) {
            $column++;
            $row = 0;
            $rows = ceil((count($this->items) - $i) / ($this->params->get('template.items_cols') - $column));
        }
        $row++;
        $i++;
    }
    else {
        // order across
        $column = $i++ % $this->params->get('template.items_cols');
    }

    if (!isset($columns[ $column ])) {
        $columns[ $column ] = '';
    }

    $columns[ $column ] .= $this->partial('item', compact('item'));
}

// render columns
$count = count($columns);
if ($count) {
    echo '<div class="items items-col-' . $count . '">';
    for ($j = 0; $j < $count; $j++) {
        $first = ($j == 0) ? ' first' : null;
        $last = ($j == $count - 1) ? ' last' : null;
        echo '<div class="width' . intval(100 / $count) . $first . $last . '">' . $columns[ $j ] . '</div>';
    }
    echo '</div>';
}

// render pagination
// echo $this->partial('pagination');



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Alphabet Pagination
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


$URI = explode('/', JURI::current());
array_splice($URI, 0, 3);
if (!isset($URI[1])) {
    $URI[1] = 1;
}
$ActivePage = $URI[1];
$CurrentPage = 0;

if ($URI[0] == 'hoi') {
    $Category = 1;
    $CategoryName = 'hoi';
}
elseif ($URI[0] == 'shlyumbergery') {
    $Category = 2;
    $CategoryName = 'shlyumbergery';
}
elseif ($URI[0] == 'decorative-deciduous') {
    $Category = 3;
    $CategoryName = 'decorative-deciduous';
}
else {
    $Category = 1;
    $CategoryName = 'hoi';
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Alphabet Pagination
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$DB = &JFactory::getDBO();
$Query = $DB->getQuery(true);
$Query->select('*');
$Query->from('#__zoo_item as item');
$Query->leftJoin('#__zoo_category_item AS category ON item.id = category.item_id');
$Query->where('category.category_id="' . $Category . '"');
$Query->where('item.state=1');
$Query->where('item.access=1');
$Query->order('item.name');
$DB->setQuery($Query);

$List = $DB->loadAssocList();
//print_r($List);
$Length = count($List);
$Pages = ceil($Length / 20);
$PagesArray = [];

$CurrentPage = 0;
for ($I = 0; $I < $Length;) {
    $List[ $I ]['name'] = str_replace('Epiphyllum', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('Ficus', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('Anthurium', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('Epipremnum', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('Epipremnum sp.', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('Maranta', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('Philodendron', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('Syngonium', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('Schlumbergera', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('Hoya sp.', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('Hoya sp. -', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('Hoya -', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('Hoya ', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('-', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = str_replace('\'', '', $List[ $I ]['name']);
    $List[ $I ]['name'] = trim($List[ $I ]['name']);


    $PagesArray[ $CurrentPage ] = utf8_encode($List[ $I ]['name'][0]);
    $I = $I + 19;


    if ($I < $Length) {
        $List[ $I ]['name'] = str_replace('Epiphyllum', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('Ficus', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('Anthurium', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('Epipremnum', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('Epipremnum sp.', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('Maranta', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('Philodendron', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('Syngonium', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('Schlumbergera', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('Hoya sp.', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('Hoya sp. -', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('Hoya -', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('Hoya ', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('-', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = str_replace('\'', '', $List[ $I ]['name']);
        $List[ $I ]['name'] = trim($List[ $I ]['name']);

        $PagesArray[ $CurrentPage ] .= ' - ' . utf8_encode($List[ $I ]['name'][0]);
        $CurrentPage++;
        $I++;
    }
    else {
        $List[ $Length - 1 ]['name'] = str_replace('Epiphyllum', '', $List[ $Length - 1 ]['name']);
        $List[ $Length - 1 ]['name'] = str_replace('Ficus', '', $List[ $Length - 1 ]['name']);
        $List[ $Length - 1 ]['name'] = str_replace('Anthurium', '', $List[ $Length - 1 ]['name']);
        $List[ $Length - 1 ]['name'] = str_replace('Epipremnum', '', $List[ $Length - 1 ]['name']);
        $List[ $Length - 1 ]['name'] = str_replace('Epipremnum sp.', '', $List[ $Length - 1 ]['name']);
        $List[ $Length - 1 ]['name'] = str_replace('Maranta', '', $List[ $Length - 1 ]['name']);
        $List[ $Length - 1 ]['name'] = str_replace('Philodendron', '', $List[ $Length - 1 ]['name']);
        $List[ $Length - 1 ]['name'] = str_replace('Syngonium', '', $List[ $Length - 1 ]['name']);
        $List[ $Length - 1 ]['name'] = str_replace('Schlumbergera', '', $List[ $Length - 1 ]['name']);
        $List[ $Length - 1 ]['name'] = str_replace('Hoya sp.', '', $List[ $Length - 1 ]['name']);
        $List[ $Length - 1 ]['name'] = str_replace('Hoya', '', $List[ $Length - 1 ]['name']);
        $List[ $Length - 1 ]['name'] = str_replace('-', '', $List[ $Length - 1 ]['name']);
        $List[ $I ]['name'] = str_replace('\'', '', $List[ $I ]['name']);
        $List[ $Length - 1 ]['name'] = trim($List[ $Length - 1 ]['name']);

        $PagesArray[ $CurrentPage ] .= ' - ' . utf8_encode($List[ $Length - 1 ]['name'][0]);
        $CurrentPage++;
        $I++;
    }
}

echo '<div class="zoo-pagination"><ul class="Pagination">';
foreach ($PagesArray as $Key => $Value) {
    if (($Key + 1) != $ActivePage) {
        echo '<li><a href="/' . $CategoryName . '/' . ($Key + 1) . '">' . ($Key + 1) . '. ' . $Value . '</a></li>';
    }
    else {
        echo '<li><span>' . ($Key + 1) . '. ' . $Value . '</span></li>';
    }

}

echo '</ul></div>';



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////